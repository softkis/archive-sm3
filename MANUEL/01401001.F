


                    MISSIONS DES INT�RIMAIRES
                    _________________________
                    
            Une mission est d�finie par 
                        une personne,
                        un client,
                        une date d�but.

            Elle contient ou peut contenir des donn�es concernant le
            salaire de l'int�rimaire et les d�tails de la facturation.

            Une personne peut travailler simultan�ment sur plusieurs
            postes avec des conditions diff�rentes.
            
            Une mission est valable jusqu'au jour o� une autre mission
            est �tablie pour la m�me personne et le m�me client.
            
            Pour des raisons de facilit�, le programme reprend toujours
            lors de la cr�ation d'une nouvelle mission les donn�es de 
            la mission la plus r�cente connue, on n'a qu'� changer les
            points relevants.

        Page 1 

            1 NUM�RO PERSONNE 
            2 NOM DE RECHERCHE

            3 POSTE DE FRAIS  = Client; � cr�er dans le fichier
                                des postes de frais

            4 DATE D�BUT      = d�but de la mission
                                ceci permet pour un m�me client et une 
                                m�me personne de red�finir chaque jour 
                                les conditions du contrat

              La touche F5 permet d'imprimer un contrat destin�
              � l'employeur - le formulaire s'appelle IN-FORM.C0
              La touche F6 permet d'imprimer un contrat destin�
              au salari� - le formulaire s'appelle IN-FORM.C1

                                
            5 D�COMPTE 
              0=HORAIRE       = La personne est pay�e � l'heure
                                introduire le salaire de base 
                                horaire sous 6
              1=MENSUEL       = La personne est pay�e au mois
                                dans ce cas il est pr�f�rable 
                                d'introduire le salaire mensuel dans
                                la carri�re, il est alors index�.

                                Si l'on introduit un salaire sous 6,
                                il pr�vaut sur la carri�re, mais 
                                il compte toujours pour l'ensemble des
                                heures saisies, quel que soit leur nombre.
            
            6 SALAIRE DE BASE = introduire le montant si n�cessaire

            7 SALAIRE DE R�F�RENCE chez le client

            8 % GRATIFICATION = introduire le pourcentage de gratification
                                calcul�s sur les gratifications retenus
                                dans le contrat collectif de l'entreprise 
                                en question.

            9 INDEXE "N" = NON  Si "N" est donn�e, le salaire n'est pas 
                                augment� en cas de tranche d'index, vaut
                                uniquement pour les salaires retenus pour
                                la mission.

           10 POSTE             Code m�tier

           11 DATE FIN 
           12 DUR�E MINIMALE JOURS de l'emploi
           13 PERIODE D'ESSAI   en jours

           14 TEXTE  1          Description de la t�che
           15        2
           16        3
           17        4

        Page 2                  DONN�ES FACTURATION ET CONTRAT

            1 MONTANT DE BASE   Taux horaire

            2 INDEXE "N" = NON  Si "N" est donn�e, le taux n'est pas 
                                augment� en cas de tranche d'index.

            3 No   CONTRAT
            4 TYPE CONTRAT      code

            5 ADRESSE MISSION
            6    "

            7 HORAIRE           texte

            8 MOTIF MISSION     code

            9 PERSONNE REMPLAC�E texte


    
