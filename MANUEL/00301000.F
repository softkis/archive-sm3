


    
    
    Pour se d�placer dans le calendrier:
        
        Haut   = fl�che en haut
        Bas    = fl�che en bas 
        Gauche = ESC    ou Ctrl + fl�che � gauche
        Droite = Enter  ou Ctrl + fl�che � droite
        
        Le programme surveille les dates d'entr�e et de d�part
        
    F2  Le programme copie les heures saisies sur la journ�e pr�c�dente.

    F3  Si l'on se place sur un premier jour et marque F3 = d�but
    F4  ensuite sur un dernier jour F4 = fin, le programme remplit
        inclusivement et de fa�on compl�mentaire les jours compris
        entre les deux.

        (Ne pas oublier d'indiquer en n�gatif "-8" les jours de maladie
        non pay�s = samedis et dimanches, avec F3=d�but et F4=fin le 
        programme proc�de automatiquement � ces indications.)

    F5  Sauvegarde des heures et retour sur la zone 0
    
    F6  Le programme remplit inclusivement et de fa�on compl�mentaire 
        tous les jours compris entre la premi�re et la derni�re journ�e
        du mois.

    F8  Effacer une journ�e

    F9  Descendre sur la ligne de commande

    Page -> Saisie des heures de nuit

        Reprendre une journ�e � partir de la ligne de commande: date

        Effacer une occupation enti�re: F8 sur la ligne de commande ou 
        sur la zone 0.

