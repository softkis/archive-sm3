


        03.01.2005

        Nouveaut�s et d�veloppements futurs en SM3
        
        Pour les modifications invisibles, comme: d�fauts de 
        programmation, proc�dures de contr�les, confort, 
        adaptations � la l�gislation etc. il suffit de dire qu'il
        ne reste aucun probl�me signal� non r�solu.
        
        Nouvelles possibilit�s, visant � faciliter le travail:
        
        1) HR saisie des heures
        
        Indemnit� de maladie non avanc� (occupations 1-9)
        En sauvegardant avec la touche F6 au lieu de F5, il est 
        possible de bloquer imm�diatement le payement, sans passer
        par MAL. Idem pour d�faire. 

        Zone 3 occupation 
        F10 acc�de imm�diatement � MAL. 
        F1 donne la situation des jours libres.

        D�part pr�matur�e: Si une personne part alors que le
        calendrier est d�j� compl�t� pour le mois, il suffit de d�finir 
        la fin du contrat dans CON et ensuite dans HR sauvegarder les 
        donn�es avec F5 (n'importe quelle occupation). Le programme corrige 
        automatiquement. Ceci fonctionne pour tous les cas de figure 
        similaires.
        
        2) O comptabilit�
        
        Pour conna�tre les valeurs non encore comptabilis�es, on
        peut �diter la compta avec: Comptabiliser = oui et d�marrer
        avec majuscule F5 ou F6. Aucun drapeau n'est mis, l'�dition est 
        toujours sur papier et porte la mention TEST dans l'ent�te.
                        
        3) LPD Liste personnes d�taill�e
        
        Elle est plus s�lective et elle permet de cr�er des fichiers
        ASCII en d�marrant avec F10 au lieu de F5 ou F6.
        
        4) DF Demande fiche d'imp�t
        
        Imprime en fran�ais ou en allemand selon la langue du salari�.
        
        5) Formulaire E106 Mot d'appel FE106
        
        6) D�compte mise au travail des jeunes
        Mot d'appel JEUNE. Attention - le salaire doit contenir le 
        code salaire 9010 pour tous les mois concern�s. 

        7) Aide embauche ch�meurs ag�s et de longue dur�e (trimestriel)
        Mot d'appel CALD. Attention - le salaire doit contenir le 
        code salaire 9011.
        
        8) Imp�ts par Multiline Mot d'appel IML

        9) Suspendre un virement dans VV
        Utile par exemple pour une saisie contest�e. Faites F2 sur
        le virement concern� (idem pour r�activer). Liste des 
        virements suspendus VS.
        
        10) Codes pays.
        
        Le programme a automatiquement install� un grand nombre de pays 
        avec leurs codes ISO, ainsi qu'un drapeau appartenance � l'UE. 
        Il faut rappeler que le CCSS ainsi que SM3 utilisent le code 
        automobile, les banques et d'autres institutions le code ISO. 
        V�rifier �ventuellement.
        Attention: Certains pays n'ont pas le code qu'on imagine
        Argentine p.ex. = RA.
        
        11) Effacer une personne  appel RIP

        Il arrive qu'une personne a �t� cr��e, mais finalement n'a pas
        commenc� son travail. Dans un cas pareil on peut effacer la 
        personne avec tous les d�tails dans la firme et l'ann�e, sans
        traces. 
        
        12) Ajuster les salaires � la pr�sence effective appel +++
        
        Il arrive qu'un personne parte alors que un/des salaires etc.
        ont �t� pr��tabli(s). Dans ce cas lancer +++ qui contr�le toute
        l'ann�e, corrige automatiquement au jour pr�s toutes les donn�es.
        y compris les donn�es comptables. Il est recommand� ensuite 
        de contr�ler avec ISOLDE les d�passements de payements.

        13) Demande d'immatriculation appel DIM

        14) Base de donn�es 
        
        En 2005 SM3 sortira un module "base de donn�es". Toutes les donn�es 
        � partir de 2002 peuvent �tre int�gr�es.
        
        Les marques principales (MySql,Oracle, Access etc.) sont support�s.
        En cas d'int�r�t, contactez Soft-kis. 

        15) SECULINE

        Comme le CCSS n'est pas plus avanc� nous n'avons en ce moment
        que la demande immatriculation et la d�claration des cotisations 
        des chambres professionnelles. Toutes les possibilit�s SECULINE
        seront support�es, sans frais suppl�mentaires de la part de 
        Soft-kis.
        
        SOFIE et CETREL. Comme vous avez �t� inform� par le CCSS 
        une commission (avec entre autres la FEDIL et les syndicats)
        a attribu� le monopole de la communication � la CETREL, ceci
        sans n�gociation et sans appel d'offres.

        Ce service co�te 350 euro par an et sera impos�, apr�s la phase 
        d'introduction, � tout le monde, garantissant ainsi � la CETREL 
        un revenu annuel d'environ 500.000 euro.

        Nous avons protest� inofficiellement au nom de nos clients,
        sans r�sultat, et nous allons pr�parer une demande officielle
        pour pouvoir offrir ce m�me service � nos clients � un prix
        beaucoup moins �lev�.

        Evidemment nous allons supporter tout utilisateur qui optera
        pour SOFIE.

        NB.: Il nous a �t� rapport� par une personne fiable, pr�sente
        aux d�bats, que quelqu'un aurait remarqu� que le co�t (350 euro) 
        serait insignifiant pour les patrons qui d�penseraient plus pour 
        une bonne soir�e. M�me si cela existe, ils n'y sont pas forc�s 
        par une commission. Nous savons aussi que des membres de la FEDIL 
        ont protest� pour la m�me raison aupr�s de celle-ci, sans 
        r�sultat.

        (J'ai parl� personnellement avec les t�moins, que je ne vais 
        cependant et en aucun cas identifier. L�on SCHOLL)



        Deuxi�me partie  - Suggestions utiles

        Mauvaises habitudes         
        
        a) Calcul inutile: Il n'y a aucune n�cessit�, apr�s la saisie des 
        heures ou apr�s une modification dans P ou n'importe quel autre
        module, de lancer un calcul dans A. Le programme remarque toute 
        modification et calcule automatiquement d�s que l'on entame une 
        autre t�che, change de personne, de mois etc.
        
        b) Comptabilit� / O : 
        
        Les donn�es destin�es � �tre comptabilis�es doivent TOUJOURS �tre 
        �dit�es avec : Comptabiliser = Oui. 
        Les pi�ces affichent alors le signe [-/-] en haut � gauche et sont 
        les seules � utiliser en comptabilit�.
       
        Ainsi le programme va m�moriser la situation comptable et peut 
        l'adapter en cas de modification. 
        
        c) Virements

        M�me si vous n'utilisez pas SM3 pour �tablir vos virements,
        il est utile de lancer le module V. Ainsi le programme peut,
        apr�s une correction, d�finir le solde � payer ou � retenir.
        Si vous ne connaissez pas de compte, mettez C = caisse, pour
        le patron ainsi que pour le salari�.
        
        Rappellons que le calcul des saisies et cessions etc. se fait 
        exclusivement par le module V.
        
        d) Sauvegarde. Chaque ann�e nous avons plusieurs cas de perte
        int�grale de donn�es. Les causes sont:
           Aucune sauvegarde des donn�es
           La sauvegarde n'a jamais fonctionn�
           Le support �tait d�fectueux. 

        Nous recommandons de proc�der � des sauvegardes r�guli�res sur 
        disques (CD ou PKZIP) et de conserver ces disques hors de 
        l'immeuble (sinistre). Les bandes magn�tiques sont � d�conseiller,
        elles sont lentes, ch�res et souvent illisibles en cas de besoin, 
        le produit n'�tant plus support�.
           
        Le cas �ch�ant Soft-kis r�installe le programme et assiste � la 
        remise en route sans frais pour l'utilisateur

        Protection des donn�es: Aussi pour des informaticiens les fichiers 
        SM3 ne peuvent pas �tre exploit�s sans l'aide du programme.


        e) Assistance t�l�phonique
        
        TROP PEU D'APPELS. Les bons clients !? Il y a des clients qui
        mettent un point d'honneur � nous appeler le moins possible,
        certains jamais. Il y a risque d'erreur, de mauvaise proc�dure,
        de frustration et nous ne sommes m�mes pas au courant.

        Amn�sie momentan�e - pas de probl�me, appellez-nous.
        
        Il n'y a pas de bricolage avec SM3. Donc, si apr�s quelques essais 
        vous n'avez toujours pas de solution, appellez-nous, vous �tes 
        probablement sur la mauvaise voie. 

        Eventuellement vous �tes tomb�s sur un probl�me o� il faudra 
        trouver une solution, ce qui nous int�resse.
        
        APPELS ABUSIFS - ceci ne concerne qu'une minorit�, mais peut
        expliquer � la majorit� pourquoi ils sont parfois confront�s
        � un interlocuteur plein d'adr�naline. Nous aussi sommes 
        seulement humains.

        (Dans ce qui suit nous n'avons rien invent� et l'inventaire
        n'est pas complet.)
        
        Nous attendons � ce que les utilisateurs aient un minimum de 
        comp�tences, �.� d. avoir suivi un cours de salaires ou de
        disposer d'une exp�rience equivalente et de conna�tre la 
        diff�rence entre un disque, un r�pertoire et un fichier. 
        
        Il est aussi utile avant de nous appeler d'allumer l'ordinateur 
        et d'appeler la situation qui pr�sente des difficult�s. 
        
        Une mise � jour ne modifie JAMAIS les salaires, par contre
        le programme m�morise chaque modification, dans le plus 
        simple d�tail, � la minute pr�s, avec l'utilisateur concern�,
        jusqu'au code salaire individuel. 
        
        Il est utile de suivre nos instructions et les mots d'appels
        que nous �pelons, existent.
        
        Quand nous donnons des instructions, il n'est pas toujours
        n�cessaire que nous attendions la fin de l'ex�cution. Il y
        a d'autres clients qui attendent � �tre servis et en cas
        d'�chec, on peut toujours rappeler.
        
        Nous ne donnons pas des cours par t�l�phone.
        
        Soft-kis n'est pas un fiduciaire sur roues. Notre support est 
        tr�s g�n�reux, nous d�clinons cependant d'�tre tenus responsables 
        pour toute erreur autre que celles dues au mauvais fonctionnement 
        du programme. 
        
        Nous ne nous consid�rons d�finitivement pas responsables si l'on 
        nous appelle un vendredi vers 17.30 heures, les salaires doivent 
        absolument sortir parce que le responsable partira en vacances le
        lendemain.
        
        Last but not least - la bonne nouvelle.
        
        Contrairement � nos conditions, aussi en 2005 nous n'avons toujours 
        pas adapt� nos prix et restent au taux d'indice de 2002 (590,84).
        
        
