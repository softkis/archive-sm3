



                           RECTIFICATION D'IMPOT
                           _____________________          
                                                                             

         Il arrive qu'une fiche d'imp�t soit remise avec un retard et que
         le recalcul des imp�ts s'impose.

         Le programme contient une routine pour rem�dier au probl�me. 

         Le principe est le suivant: Les imp�ts des mois pr�c�dents sont 
         recalcul�s, la diff�rence nette, positive ou n�gative, est ajout�e 
         resp. d�duite du salaire du mois en cours ainsi que de tous les 
         salaires pr�c�dents modifi�s. Les indemnit�s pour maladie sont
         consid�r�es si elles ont �t� avanc�es par l'employeur.

         La manipulation se pr�sente sur la fiche de paie sous forme de 
         codes salaires sp�ciaux indiquant clairement, par mois, les 
         montants nets corrig�s.
         
         La proc�dure en deux �tapes:
         
         a) Il faut se mettre sur le mois en cours, c'est � dire le prochain
         salaire � payer. Dans le programme "Fiches d'imp�t" (FICHE) la fiche 
         est mise � jour, avec les mois d'application correctes.

         b) Dans le programme "Rectification d'imp�t" (RI) on appelle la
         personne en question et on d�marre la proc�dure de correction
         avec la touche F5. C'est tout.
         
         Le programme automatiquement tient compte de ces modifications 
         dans la mise � jour pour la comptabilit� ainsi que dans la
         d�claration des imp�ts sur salaires.
         
         La proc�dure peut �tre lanc�e avant ou apr�s l'�tablissement des 
         salaires, mais �videmment avant les virements. 

         (Le programme le permet: on risque de se retrouver avec un solde 
         qu'il faudra de nouveau compenser avec le salaire du mois suivant 
         ou, si le montant est positif, par un virement pour solde.)
         
         
         