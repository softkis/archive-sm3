

        Signal�tique client
        ___________________ 

        Page 1

         1 NUM�RO CLIENT            F6 donne le prochain num�ro disponible
         2 NOM                                  
         3 CODE ALPHA               Nom de recherche alphanum�rique (Matchcode)

         4 CODE POSTAL              Pour les r�sidents du Grand-Duch�:
                                    si les champs RUE et LOCALIT� sont vides,
                                    ils sont compl�t�s par le programme
                                    F5 = reprise du fichier original
         5 NUM�RO                   de la maison, alphanum�rique
         6 RUE                      peut �tre sur�crit
         7 LOCALIT�                 peut �tre sur�crit

         8 T�L�PHONE                      
         9 FAX  
        10 RESPONSABLE              appara�t sur certains documents

        11 MATRICULE                AAAA 
        12 NO                       NNNN 
        13 SNOCS                    code 3 chiffres / test de coh�rence /
                                    peut �tre provisoirement laiss� � z�ro
                                    
        15 CODE TVA IBLC     
        
        16 ACTIVIT�                 de l'entreprise

        Page 2

         1 COMP�TENCE               0-9 niveau de confidentialit�, l'utilisateur
                                    doit disposer d'un niveau de comp�tence 
                                    au moins �quivalant pour pouvoir traiter
                                    ce client

         2 MOT DE PASSE             encrypt�
        
         3 PAYS
         4 DATE D�BUT               Pour fiduciaires  / d�but des relations 
         5 DATE FIN                 Fin des relations

         6 ADRESSE COURRIER 1 RUE
         7 ADRESSE COURRIER 2 LOCALIT�
         8 RESPONSABLE
         9 REMARQUE
