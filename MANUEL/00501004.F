


            Contr�le / Cr�ation / Effacement d'acomptes
            �������������������������������������������
            
            Ce module permet de g�rer rapidement les acomptes type 1
            en affichant les valeurs qui �ventuellement peuvent
            influencer la valeur o� l'existence:
            
            - Le montant d'acompte existant 
            - Le net � payer restant
            - Les heures d'absence pour maladie
            - La date de sortie
            
            La touche F5 sur les zones 1 & 2 permet de rechercher
            rapidement toutes les personnes qui ont:
            
            - une date de sortie dans le mois
            - des heures de maladie � la charge des caisse de maladie
            - un net � payer n�gatif
            
            La touche F5 sur la zone 3 = acompte permet de sauvegarder
            imm�diatement la nouvelle valeur avec recalcul automatique
            du salaire.
            
            A noter que le code salaire de valeur z�ro n'est pas effac� 
            pour �viter qu'il soit r�tabli accidentellement par un code 
            salaire fixe correspondant.
            
            