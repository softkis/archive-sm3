      ****************************
      * parameter for PRINT-PAGE *
      ****************************
       01  CRE-CONVERSE-DATA.
           05  CRE-RET-CODE       
                                       PIC S9(4) COMP-1.
           05  CRE-LENS.
               10  CRE-LEN-LEN    
                                       PIC S9(4) COMP-1 VALUE +20.
               10  CRE-IP-NUM-LEN 
                                       PIC S9(4) COMP-1 VALUE +40.
               10  CRE-IP-CHAR-LEN
                                       PIC S9(4) COMP-1 VALUE +104.
               10  CRE-OP-NUM-LEN 
                                       PIC S9(4) COMP-1 VALUE +6.
               10  CRE-OP-CHAR-LEN  
                                       PIC S9(4) COMP-1 VALUE +2.
               10  CRE-FIELD-LEN  
                                       PIC S9(4) COMP-1 VALUE +2066.
               10  CRE-COLR-LEN   
                                       PIC S9(4) COMP-1 VALUE +140.
               10  FILLER          
                                       PIC S9(4) COMP-1 VALUE +0.
               10  FILLER           
                                       PIC S9(4) COMP-1 VALUE +0.
               10  FILLER          
                                       PIC S9(4) COMP-1 VALUE +0.
           05  CRE-DATA.
      ******** CRE-IP-NUM-DATA ********
               10  CRE-KEY        
                                       PIC S9(4) COMP-1.
               10  FILLER              PIC X(38).
      ******** CRE-IP-CHAR-DATA *******
               10  CRE-NEXT-PANEL 
                                       PIC X(8).
               10  FILLER              PIC X(94).
               10  CRE-WAIT-SW    
                                       PIC X.
               10  FILLER              PIC X.
      ******** CRE-OP-NUM-DATA ********
               10  CRE-PAN-POS-SW 
                                       PIC S9(4) COMP-1.
               10  CRE-PAN-ROW    
                                       PIC S9(4) COMP-1.
               10  CRE-PAN-COL    
                                       PIC S9(4) COMP-1.
      ******** CRE-OP-CHAR-DATA ********
               10  FILLER              PIC X(2).
      ******** CRE-OP-VAR-DATA ********
           05  CRE-FIELDS.
               10  CRE-LOCCDU 
                                       PIC X(0013).
               10  CRE-LOCCAU 
                                       PIC X(0013).
               10  CRE-LANNEE 
                                       PIC X(0004).
               10  CRE-OCCDU 
                                       PIC X(0013).
               10  CRE-OCCAU 
                                       PIC X(0013).
               10  CRE-ANNEE 
                                       PIC X(0004).
               10  CRE-LMATR 
                                       PIC X(0012).
               10  CRE-NO-LFICHE 
                                       PIC X(0010).
               10  CRE-MATR 
                                       PIC X(0012).
               10  CRE-NO-FICHE 
                                       PIC X(0010).
               10  CRE-FIELD-ID-86 
                                       PIC X(0010).
               10  CRE-FIELD-ID-87 
                                       PIC X(0010).
               10  CRE-LV01 
                                       PIC X(0011).
               10  CRE-FIELD-ID-2 
                                       PIC X(0010).
               10  CRE-FIELD-ID-1 
                                       PIC X(0010).
               10  CRE-LV01 
                                       PIC X(0011).
               10  CRE-LV02 
                                       PIC X(0011).
               10  FILLER OCCURS 4.
                   15  CRE-LPERS-ADR 
                                       PIC X(0060).
               10  CRE-LV02 
                                       PIC X(0011).
               10  FILLER OCCURS 4.
                   15  CRE-LPERS-ADR 
                                       PIC X(0060).
               10  CRE-LV03 
                                       PIC X(0011).
               10  CRE-LV03 
                                       PIC X(0011).
               10  CRE-LV04 
                                       PIC X(0011).
               10  CRE-LV04 
                                       PIC X(0011).
               10  CRE-LV05 
                                       PIC X(0011).
               10  CRE-LV05 
                                       PIC X(0011).
               10  CRE-LV06 
                                       PIC X(0011).
               10  CRE-LV06 
                                       PIC X(0011).
               10  CRE-LIMP-CL 
                                       PIC X(0005).
               10  CRE-LIMP-TX 
                                       PIC X(0005).
               10  CRE-LIMP-CL 
                                       PIC X(0005).
               10  CRE-LIMP-TX 
                                       PIC X(0005).
               10  CRE-LV07 
                                       PIC X(0011).
               10  CRE-LV07 
                                       PIC X(0011).
               10  CRE-LV08 
                                       PIC X(0011).
               10  CRE-LV08 
                                       PIC X(0011).
               10  CRE-FIELD-ID-77 
                                       PIC X(0010).
               10  CRE-LV09 
                                       PIC X(0011).
               10  CRE-LFIRM-TVA 
                                       PIC X(0020).
               10  CRE-FIELD-ID-13 
                                       PIC X(0010).
               10  CRE-LV09 
                                       PIC X(0011).
               10  CRE-LFIRM-TVA 
                                       PIC X(0020).
               10  CRE-FIELD-ID-78 
                                       PIC X(0010).
               10  CRE-LV10 
                                       PIC X(0011).
               10  CRE-LV11 
                                       PIC X(0011).
               10  CRE-FIELD-ID-16 
                                       PIC X(0010).
               10  CRE-LV10 
                                       PIC X(0011).
               10  CRE-LV11 
                                       PIC X(0011).
               10  CRE-FIELD-ID-79 
                                       PIC X(0010).
               10  CRE-LV11 
                                       PIC X(0011).
               10  CRE-FIELD-ID-20 
                                       PIC X(0010).
               10  CRE-LV11 
                                       PIC X(0011).
               10  FILLER OCCURS 4.
                   15  CRE-LFIRM-ADR 
                                       PIC X(0030).
               10  FILLER OCCURS 4.
                   15  CRE-LFIRM-ADR 
                                       PIC X(0030).
               10  CRE-FIELD-ID-80 
                                       PIC X(0010).
               10  CRE-LV12 
                                       PIC X(0011).
               10  CRE-FIELD-ID-22 
                                       PIC X(0010).
               10  CRE-LV12 
                                       PIC X(0011).
               10  CRE-FIELD-ID-81 
                                       PIC X(0010).
               10  CRE-LV13 
                                       PIC X(0011).
               10  CRE-FIELD-ID-24 
                                       PIC X(0010).
               10  CRE-LV13 
                                       PIC X(0011).
               10  CRE-FIELD-ID-82 
                                       PIC X(0010).
               10  CRE-LV14 
                                       PIC X(0011).
               10  CRE-FIELD-ID-26 
                                       PIC X(0010).
               10  CRE-LV14 
                                       PIC X(0011).
               10  CRE-FIELD-ID-83 
                                       PIC X(0010).
               10  CRE-LV15 
                                       PIC X(0011).
               10  CRE-FIELD-ID-28 
                                       PIC X(0010).
               10  CRE-LV15 
                                       PIC X(0011).
               10  CRE-LV16 
                                       PIC X(0011).
               10  CRE-LFIRM-TEL 
                                       PIC X(0020).
               10  CRE-LV16 
                                       PIC X(0011).
               10  CRE-LFIRM-TEL 
                                       PIC X(0020).
               10  CRE-LV17 
                                       PIC X(0011).
               10  CRE-LTODAY 
                                       PIC X(0010).
               10  CRE-LV17 
                                       PIC X(0011).
               10  CRE-LTODAY 
                                       PIC X(0010).
               10  CRE-LV18 
                                       PIC X(0011).
               10  CRE-LV18 
                                       PIC X(0011).
               10  CRE-LV19 
                                       PIC X(0011).
               10  CRE-LV19 
                                       PIC X(0011).
               10  CRE-LV20 
                                       PIC X(0011).
               10  CRE-LV20 
                                       PIC X(0011).
               10  CRE-LV21 
                                       PIC X(0011).
               10  CRE-LV21 
                                       PIC X(0011).
               10  CRE-LV22 
                                       PIC X(0011).
               10  CRE-LV22 
                                       PIC X(0011).
               10  CRE-LFIDU-NOM 
                                       PIC X(0030).
               10  CRE-LFIDU-NOM 
                                       PIC X(0030).
               10  CRE-LV23 
                                       PIC X(0011).
               10  CRE-LV23 
                                       PIC X(0011).
               10  CRE-LFIDU-TEL 
                                       PIC X(0020).
               10  CRE-LFIDU-TEL 
                                       PIC X(0020).
               10  CRE-LV25 
                                       PIC X(0011).
               10  CRE-LV24 
                                       PIC X(0011).
               10  CRE-LV25 
                                       PIC X(0011).
               10  CRE-LV24 
                                       PIC X(0011).
               10  CRE-FIELD-ID-94 
                                       PIC X(0010).
               10  CRE-FIELD-ID-95 
                                       PIC X(0010).
               10  CRE-FIELD-ID-47 
                                       PIC X(0010).
               10  CRE-FIELD-ID-140 
                                       PIC X(0010).
               10  CRE-FIELD-ID-49 
                                       PIC X(0010).
               10  FILLER OCCURS 3.
                   15  CRE-LPI-DU 
                                       PIC X(0013).
               10  FILLER OCCURS 3.
                   15  CRE-LPI-AU 
                                       PIC X(0013).
               10  FILLER OCCURS 3.
                   15  CRE-LPI-DU 
                                       PIC X(0013).
               10  FILLER OCCURS 3.
                   15  CRE-LPI-AU 
                                       PIC X(0013).
               10  CRE-LCM-YN 
                                       PIC X(0005).
               10  CRE-CM-YN 
                                       PIC X(0005).
               10  CRE-LDEC-OUI 
                                       PIC X(0001).
               10  CRE-LDEC-NON 
                                       PIC X(0001).
               10  CRE-FIELD-ID-50 
                                       PIC X(0010).
               10  CRE-FIELD-ID-52 
                                       PIC X(0010).
               10  CRE-FIELD-ID-51 
                                       PIC X(0010).
               10  CRE-LDEC-OUI 
                                       PIC X(0001).
               10  CRE-LDEC-NON 
                                       PIC X(0001).
               10  CRE-FIELD-ID-96 
                                       PIC X(0010).
               10  CRE-FIELD-ID-48 
                                       PIC X(0010).
           05  CRE-COLRS.
               10  CRE-LOCCDU-C      
                                       PIC X.
               10  CRE-LOCCAU-C      
                                       PIC X.
               10  CRE-LANNEE-C      
                                       PIC X.
               10  CRE-OCCDU-C      
                                       PIC X.
               10  CRE-OCCAU-C      
                                       PIC X.
               10  CRE-ANNEE-C      
                                       PIC X.
               10  CRE-LMATR-C      
                                       PIC X.
               10  CRE-NO-LFICHE-C      
                                       PIC X.
               10  CRE-MATR-C      
                                       PIC X.
               10  CRE-NO-FICHE-C      
                                       PIC X.
               10  CRE-FIELD-ID-86-C      
                                       PIC X.
               10  CRE-FIELD-ID-87-C      
                                       PIC X.
               10  CRE-LV01-C      
                                       PIC X.
               10  CRE-FIELD-ID-2-C      
                                       PIC X.
               10  CRE-FIELD-ID-1-C      
                                       PIC X.
               10  CRE-LV01-C      
                                       PIC X.
               10  CRE-LV02-C      
                                       PIC X.
               10  FILLER OCCURS 4.
                   15  CRE-LPERS-ADR-C 
                                       PIC X.
               10  CRE-LV02-C      
                                       PIC X.
               10  FILLER OCCURS 4.
                   15  CRE-LPERS-ADR-C 
                                       PIC X.
               10  CRE-LV03-C      
                                       PIC X.
               10  CRE-LV03-C      
                                       PIC X.
               10  CRE-LV04-C      
                                       PIC X.
               10  CRE-LV04-C      
                                       PIC X.
               10  CRE-LV05-C      
                                       PIC X.
               10  CRE-LV05-C      
                                       PIC X.
               10  CRE-LV06-C      
                                       PIC X.
               10  CRE-LV06-C      
                                       PIC X.
               10  CRE-LIMP-CL-C      
                                       PIC X.
               10  CRE-LIMP-TX-C      
                                       PIC X.
               10  CRE-LIMP-CL-C      
                                       PIC X.
               10  CRE-LIMP-TX-C      
                                       PIC X.
               10  CRE-LV07-C      
                                       PIC X.
               10  CRE-LV07-C      
                                       PIC X.
               10  CRE-LV08-C      
                                       PIC X.
               10  CRE-LV08-C      
                                       PIC X.
               10  CRE-FIELD-ID-77-C      
                                       PIC X.
               10  CRE-LV09-C      
                                       PIC X.
               10  CRE-LFIRM-TVA-C      
                                       PIC X.
               10  CRE-FIELD-ID-13-C      
                                       PIC X.
               10  CRE-LV09-C      
                                       PIC X.
               10  CRE-LFIRM-TVA-C      
                                       PIC X.
               10  CRE-FIELD-ID-78-C      
                                       PIC X.
               10  CRE-LV10-C      
                                       PIC X.
               10  CRE-LV11-C      
                                       PIC X.
               10  CRE-FIELD-ID-16-C      
                                       PIC X.
               10  CRE-LV10-C      
                                       PIC X.
               10  CRE-LV11-C      
                                       PIC X.
               10  CRE-FIELD-ID-79-C      
                                       PIC X.
               10  CRE-LV11-C      
                                       PIC X.
               10  CRE-FIELD-ID-20-C      
                                       PIC X.
               10  CRE-LV11-C      
                                       PIC X.
               10  FILLER OCCURS 4.
                   15  CRE-LFIRM-ADR-C 
                                       PIC X.
               10  FILLER OCCURS 4.
                   15  CRE-LFIRM-ADR-C 
                                       PIC X.
               10  CRE-FIELD-ID-80-C      
                                       PIC X.
               10  CRE-LV12-C      
                                       PIC X.
               10  CRE-FIELD-ID-22-C      
                                       PIC X.
               10  CRE-LV12-C      
                                       PIC X.
               10  CRE-FIELD-ID-81-C      
                                       PIC X.
               10  CRE-LV13-C      
                                       PIC X.
               10  CRE-FIELD-ID-24-C      
                                       PIC X.
               10  CRE-LV13-C      
                                       PIC X.
               10  CRE-FIELD-ID-82-C      
                                       PIC X.
               10  CRE-LV14-C      
                                       PIC X.
               10  CRE-FIELD-ID-26-C      
                                       PIC X.
               10  CRE-LV14-C      
                                       PIC X.
               10  CRE-FIELD-ID-83-C      
                                       PIC X.
               10  CRE-LV15-C      
                                       PIC X.
               10  CRE-FIELD-ID-28-C      
                                       PIC X.
               10  CRE-LV15-C      
                                       PIC X.
               10  CRE-LV16-C      
                                       PIC X.
               10  CRE-LFIRM-TEL-C      
                                       PIC X.
               10  CRE-LV16-C      
                                       PIC X.
               10  CRE-LFIRM-TEL-C      
                                       PIC X.
               10  CRE-LV17-C      
                                       PIC X.
               10  CRE-LTODAY-C      
                                       PIC X.
               10  CRE-LV17-C      
                                       PIC X.
               10  CRE-LTODAY-C      
                                       PIC X.
               10  CRE-LV18-C      
                                       PIC X.
               10  CRE-LSIG-C      
                                       PIC X.
               10  CRE-LV18-C      
                                       PIC X.
               10  CRE-LSIG-C      
                                       PIC X.
               10  CRE-LV19-C      
                                       PIC X.
               10  CRE-LV19-C      
                                       PIC X.
               10  CRE-LV20-C      
                                       PIC X.
               10  CRE-LV20-C      
                                       PIC X.
               10  CRE-LV21-C      
                                       PIC X.
               10  CRE-LV21-C      
                                       PIC X.
               10  CRE-LV22-C      
                                       PIC X.
               10  CRE-LV22-C      
                                       PIC X.
               10  CRE-LFIDU-NOM-C      
                                       PIC X.
               10  CRE-LFIDU-NOM-C      
                                       PIC X.
               10  CRE-LV23-C      
                                       PIC X.
               10  CRE-LV23-C      
                                       PIC X.
               10  CRE-LFIDU-TEL-C      
                                       PIC X.
               10  CRE-LFIDU-TEL-C      
                                       PIC X.
               10  CRE-LV25-C      
                                       PIC X.
               10  CRE-LV24-C      
                                       PIC X.
               10  CRE-LV25-C      
                                       PIC X.
               10  CRE-LV24-C      
                                       PIC X.
               10  CRE-FIELD-ID-94-C      
                                       PIC X.
               10  CRE-FIELD-ID-95-C      
                                       PIC X.
               10  CRE-FIELD-ID-47-C      
                                       PIC X.
               10  CRE-FIELD-ID-140-C      
                                       PIC X.
               10  CRE-FIELD-ID-49-C      
                                       PIC X.
               10  FILLER OCCURS 3.
                   15  CRE-LPI-DU-C 
                                       PIC X.
               10  FILLER OCCURS 3.
                   15  CRE-LPI-AU-C 
                                       PIC X.
               10  FILLER OCCURS 3.
                   15  CRE-LPI-DU-C 
                                       PIC X.
               10  FILLER OCCURS 3.
                   15  CRE-LPI-AU-C 
                                       PIC X.
               10  CRE-LCM-YN-C      
                                       PIC X.
               10  CRE-CM-YN-C      
                                       PIC X.
               10  CRE-LDEC-OUI-C      
                                       PIC X.
               10  CRE-LDEC-NON-C      
                                       PIC X.
               10  CRE-FIELD-ID-50-C      
                                       PIC X.
               10  CRE-FIELD-ID-52-C      
                                       PIC X.
               10  CRE-FIELD-ID-51-C      
                                       PIC X.
               10  CRE-LDEC-OUI-C      
                                       PIC X.
               10  CRE-LDEC-NON-C      
                                       PIC X.
               10  CRE-FIELD-ID-96-C      
                                       PIC X.
               10  CRE-FIELD-ID-48-C      
                                       PIC X.
      ********************************************
      * field ids - use for field identification *
      ********************************************
       01  CRE-IDS.
               05  CRE-LOCCDU-I
                                   PIC S9(4) COMP-1 VALUE +111.
               05  CRE-LOCCAU-I
                                   PIC S9(4) COMP-1 VALUE +112.
               05  CRE-LANNEE-I
                                   PIC S9(4) COMP-1 VALUE +113.
               05  CRE-OCCDU-I
                                   PIC S9(4) COMP-1 VALUE +97.
               05  CRE-OCCAU-I
                                   PIC S9(4) COMP-1 VALUE +98.
               05  CRE-ANNEE-I
                                   PIC S9(4) COMP-1 VALUE +99.
               05  CRE-LMATR-I
                                   PIC S9(4) COMP-1 VALUE +114.
               05  CRE-NO-LFICHE-I
                                   PIC S9(4) COMP-1 VALUE +131.
               05  CRE-MATR-I
                                   PIC S9(4) COMP-1 VALUE +102.
               05  CRE-NO-FICHE-I
                                   PIC S9(4) COMP-1 VALUE +130.
               05  CRE-FIELD-ID-86-I
                                   PIC S9(4) COMP-1 VALUE +86.
               05  CRE-FIELD-ID-87-I
                                   PIC S9(4) COMP-1 VALUE +87.
               05  CRE-LV01-I
                                   PIC S9(4) COMP-1 VALUE +18.
               05  CRE-FIELD-ID-2-I
                                   PIC S9(4) COMP-1 VALUE +2.
               05  CRE-FIELD-ID-1-I
                                   PIC S9(4) COMP-1 VALUE +1.
               05  CRE-LV01-I
                                   PIC S9(4) COMP-1 VALUE +3.
               05  CRE-LV02-I
                                   PIC S9(4) COMP-1 VALUE +61.
               05  CRE-LPERS-ADR-I
                                   PIC S9(4) COMP-1 VALUE +115.
               05  CRE-LV02-I
                                   PIC S9(4) COMP-1 VALUE +4.
               05  CRE-LPERS-ADR-I
                                   PIC S9(4) COMP-1 VALUE +103.
               05  CRE-LV03-I
                                   PIC S9(4) COMP-1 VALUE +62.
               05  CRE-LV03-I
                                   PIC S9(4) COMP-1 VALUE +5.
               05  CRE-LV04-I
                                   PIC S9(4) COMP-1 VALUE +63.
               05  CRE-LV04-I
                                   PIC S9(4) COMP-1 VALUE +6.
               05  CRE-LV05-I
                                   PIC S9(4) COMP-1 VALUE +64.
               05  CRE-LV05-I
                                   PIC S9(4) COMP-1 VALUE +7.
               05  CRE-LV06-I
                                   PIC S9(4) COMP-1 VALUE +65.
               05  CRE-LV06-I
                                   PIC S9(4) COMP-1 VALUE +8.
               05  CRE-LIMP-CL-I
                                   PIC S9(4) COMP-1 VALUE +60.
               05  CRE-LIMP-TX-I
                                   PIC S9(4) COMP-1 VALUE +116.
               05  CRE-LIMP-CL-I
                                   PIC S9(4) COMP-1 VALUE +9.
               05  CRE-LIMP-TX-I
                                   PIC S9(4) COMP-1 VALUE +10.
               05  CRE-LV07-I
                                   PIC S9(4) COMP-1 VALUE +66.
               05  CRE-LV07-I
                                   PIC S9(4) COMP-1 VALUE +11.
               05  CRE-LV08-I
                                   PIC S9(4) COMP-1 VALUE +67.
               05  CRE-LV08-I
                                   PIC S9(4) COMP-1 VALUE +12.
               05  CRE-FIELD-ID-77-I
                                   PIC S9(4) COMP-1 VALUE +77.
               05  CRE-LV09-I
                                   PIC S9(4) COMP-1 VALUE +68.
               05  CRE-LFIRM-TVA-I
                                   PIC S9(4) COMP-1 VALUE +117.
               05  CRE-FIELD-ID-13-I
                                   PIC S9(4) COMP-1 VALUE +13.
               05  CRE-LV09-I
                                   PIC S9(4) COMP-1 VALUE +14.
               05  CRE-LFIRM-TVA-I
                                   PIC S9(4) COMP-1 VALUE +15.
               05  CRE-FIELD-ID-78-I
                                   PIC S9(4) COMP-1 VALUE +78.
               05  CRE-LV10-I
                                   PIC S9(4) COMP-1 VALUE +69.
               05  CRE-LV11-I
                                   PIC S9(4) COMP-1 VALUE +70.
               05  CRE-FIELD-ID-16-I
                                   PIC S9(4) COMP-1 VALUE +16.
               05  CRE-LV10-I
                                   PIC S9(4) COMP-1 VALUE +17.
               05  CRE-LV11-I
                                   PIC S9(4) COMP-1 VALUE +19.
               05  CRE-FIELD-ID-79-I
                                   PIC S9(4) COMP-1 VALUE +79.
               05  CRE-LV11-I
                                   PIC S9(4) COMP-1 VALUE +71.
               05  CRE-FIELD-ID-20-I
                                   PIC S9(4) COMP-1 VALUE +20.
               05  CRE-LV11-I
                                   PIC S9(4) COMP-1 VALUE +21.
               05  CRE-LFIRM-ADR-I
                                   PIC S9(4) COMP-1 VALUE +88.
               05  CRE-LFIRM-ADR-I
                                   PIC S9(4) COMP-1 VALUE +109.
               05  CRE-FIELD-ID-80-I
                                   PIC S9(4) COMP-1 VALUE +80.
               05  CRE-LV12-I
                                   PIC S9(4) COMP-1 VALUE +72.
               05  CRE-FIELD-ID-22-I
                                   PIC S9(4) COMP-1 VALUE +22.
               05  CRE-LV12-I
                                   PIC S9(4) COMP-1 VALUE +23.
               05  CRE-FIELD-ID-81-I
                                   PIC S9(4) COMP-1 VALUE +81.
               05  CRE-LV13-I
                                   PIC S9(4) COMP-1 VALUE +73.
               05  CRE-FIELD-ID-24-I
                                   PIC S9(4) COMP-1 VALUE +24.
               05  CRE-LV13-I
                                   PIC S9(4) COMP-1 VALUE +25.
               05  CRE-FIELD-ID-82-I
                                   PIC S9(4) COMP-1 VALUE +82.
               05  CRE-LV14-I
                                   PIC S9(4) COMP-1 VALUE +119.
               05  CRE-FIELD-ID-26-I
                                   PIC S9(4) COMP-1 VALUE +26.
               05  CRE-LV14-I
                                   PIC S9(4) COMP-1 VALUE +27.
               05  CRE-FIELD-ID-83-I
                                   PIC S9(4) COMP-1 VALUE +83.
               05  CRE-LV15-I
                                   PIC S9(4) COMP-1 VALUE +76.
               05  CRE-FIELD-ID-28-I
                                   PIC S9(4) COMP-1 VALUE +28.
               05  CRE-LV15-I
                                   PIC S9(4) COMP-1 VALUE +29.
               05  CRE-LV16-I
                                   PIC S9(4) COMP-1 VALUE +84.
               05  CRE-LFIRM-TEL-I
                                   PIC S9(4) COMP-1 VALUE +118.
               05  CRE-LV16-I
                                   PIC S9(4) COMP-1 VALUE +30.
               05  CRE-LFIRM-TEL-I
                                   PIC S9(4) COMP-1 VALUE +31.
               05  CRE-LV17-I
                                   PIC S9(4) COMP-1 VALUE +134.
               05  CRE-LTODAY-I
                                   PIC S9(4) COMP-1 VALUE +120.
               05  CRE-LV17-I
                                   PIC S9(4) COMP-1 VALUE +32.
               05  CRE-LTODAY-I
                                   PIC S9(4) COMP-1 VALUE +33.
               05  CRE-LV18-I
                                   PIC S9(4) COMP-1 VALUE +55.
               05  CRE-LSIG-I
                                   PIC S9(4) COMP-1 VALUE +132.
               05  CRE-LV18-I
                                   PIC S9(4) COMP-1 VALUE +34.
               05  CRE-LSIG-I
                                   PIC S9(4) COMP-1 VALUE +35.
               05  CRE-LV19-I
                                   PIC S9(4) COMP-1 VALUE +74.
               05  CRE-LV19-I
                                   PIC S9(4) COMP-1 VALUE +36.
               05  CRE-LV20-I
                                   PIC S9(4) COMP-1 VALUE +75.
               05  CRE-LV20-I
                                   PIC S9(4) COMP-1 VALUE +37.
               05  CRE-LV21-I
                                   PIC S9(4) COMP-1 VALUE +85.
               05  CRE-LV21-I
                                   PIC S9(4) COMP-1 VALUE +38.
               05  CRE-LV22-I
                                   PIC S9(4) COMP-1 VALUE +137.
               05  CRE-LV22-I
                                   PIC S9(4) COMP-1 VALUE +40.
               05  CRE-LFIDU-NOM-I
                                   PIC S9(4) COMP-1 VALUE +121.
               05  CRE-LFIDU-NOM-I
                                   PIC S9(4) COMP-1 VALUE +41.
               05  CRE-LV23-I
                                   PIC S9(4) COMP-1 VALUE +138.
               05  CRE-LV23-I
                                   PIC S9(4) COMP-1 VALUE +42.
               05  CRE-LFIDU-TEL-I
                                   PIC S9(4) COMP-1 VALUE +122.
               05  CRE-LFIDU-TEL-I
                                   PIC S9(4) COMP-1 VALUE +43.
               05  CRE-LV25-I
                                   PIC S9(4) COMP-1 VALUE +56.
               05  CRE-LV24-I
                                   PIC S9(4) COMP-1 VALUE +139.
               05  CRE-LV25-I
                                   PIC S9(4) COMP-1 VALUE +45.
               05  CRE-LV24-I
                                   PIC S9(4) COMP-1 VALUE +46.
               05  CRE-FIELD-ID-94-I
                                   PIC S9(4) COMP-1 VALUE +94.
               05  CRE-FIELD-ID-95-I
                                   PIC S9(4) COMP-1 VALUE +95.
               05  CRE-FIELD-ID-47-I
                                   PIC S9(4) COMP-1 VALUE +47.
               05  CRE-FIELD-ID-140-I
                                   PIC S9(4) COMP-1 VALUE +140.
               05  CRE-FIELD-ID-49-I
                                   PIC S9(4) COMP-1 VALUE +49.
               05  CRE-LPI-DU-I
                                   PIC S9(4) COMP-1 VALUE +123.
               05  CRE-LPI-AU-I
                                   PIC S9(4) COMP-1 VALUE +126.
               05  CRE-LPI-DU-I
                                   PIC S9(4) COMP-1 VALUE +39.
               05  CRE-LPI-AU-I
                                   PIC S9(4) COMP-1 VALUE +54.
               05  CRE-LCM-YN-I
                                   PIC S9(4) COMP-1 VALUE +129.
               05  CRE-CM-YN-I
                                   PIC S9(4) COMP-1 VALUE +110.
               05  CRE-LDEC-OUI-I
                                   PIC S9(4) COMP-1 VALUE +104.
               05  CRE-LDEC-NON-I
                                   PIC S9(4) COMP-1 VALUE +107.
               05  CRE-FIELD-ID-50-I
                                   PIC S9(4) COMP-1 VALUE +50.
               05  CRE-FIELD-ID-52-I
                                   PIC S9(4) COMP-1 VALUE +52.
               05  CRE-FIELD-ID-51-I
                                   PIC S9(4) COMP-1 VALUE +51.
               05  CRE-LDEC-OUI-I
                                   PIC S9(4) COMP-1 VALUE +92.
               05  CRE-LDEC-NON-I
                                   PIC S9(4) COMP-1 VALUE +93.
               05  CRE-FIELD-ID-96-I
                                   PIC S9(4) COMP-1 VALUE +96.
               05  CRE-FIELD-ID-48-I
                                   PIC S9(4) COMP-1 VALUE +48.
