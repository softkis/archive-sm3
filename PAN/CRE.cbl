       IDENTIFICATION DIVISION.
       PROGRAM-ID. CRE.

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER. IBM-PC.
       OBJECT-COMPUTER. IBM-PC.

       DATA DIVISION.
       WORKING-STORAGE SECTION .

       COPY "QPR.CPY".
       COPY "CRE.CPY".

      *******************
      * HOME MADE STUFF
      *******************

       01  WDEVICE-TABLE.
           02  WS1-DEVICES         PIC X(80) OCCURS 10.
       01  WS1-OFFSET              PIC 9(4).
       01  WS1-I                   PIC 9(4).

      *********************
       PROCEDURE DIVISION.
      *********************

       MAIN-LINE.

           PERFORM INIT.
           PERFORM SELECT-PRINTER.
           PERFORM PRINT-PAGES.
           PERFORM END-PRINT.

           STOP RUN.

      **************
       INIT.
      **************

           MOVE LOW-VALUES TO QPR-INIT-AREA.

           MOVE "n" TO QPR-METHOD.
           MOVE "E:\SM3\PAN\CERTREM.PAN" TO QPR-FILE.

           CALL "QPR" USING
                      QPR-INIT
                      QPR-INIT-AREA.

           IF QPR-QI-RET NOT = 0
               STOP RUN
           END-IF.

      ***********************
       SELECT-PRINTER.
      ***********************

           MOVE    LOW-VALUES    TO  QPR-AREA.

           MOVE "Document 1"     TO  QPR-DOC-NAME.
           MOVE "p"              TO  QPR-ORIENTATION.
           MOVE "y"              TO  QPR-DIALOG.
                                   
           CALL "QPR"   USING  QPR-SELECT-PRINTER
                               QPR-AREA.

      * reset qpr-dialog to "s" above if you want a specific printer

           IF  QPR-DIALOG = "s"
               PERFORM SELECT-FROM-LIST
           END-IF.

      **************************
       SELECT-FROM-LIST.
      **************************

           IF  QPR-RET-CODE = -10
               MOVE 1 TO WS1-I
               MOVE 1 TO WS1-OFFSET
               PERFORM UNSTRING-DEVICE-LIST UNTIL WS1-I > 10
               MOVE 1 TO WS1-I
               PERFORM SELECT-ONE-DEVICE UNTIL WS1-I > 10
           ELSE
               MOVE "y" TO QPR-DIALOG
           END-IF.

           CALL "QPR"  USING  QPR-SELECT-PRINTER
                              QPR-AREA.

      ******************************
       UNSTRING-DEVICE-LIST.
      ******************************

           UNSTRING QPR-DEVICE-LIST DELIMITED BY ';' INTO
                    WS1-DEVICES(WS1-I) WITH POINTER WS1-OFFSET.
           ADD     1               TO  WS1-I.

      ***************************
       SELECT-ONE-DEVICE.
      ***************************

           IF WS1-DEVICES (WS1-I) (1 : 11) = "HP LaserJet"
               MOVE LOW-VALUE          TO  QPR-DEVICE-LIST
               STRING WS1-DEVICES(WS1-I) DELIMITED BY SIZE
                  INTO QPR-DEVICE-LIST
               MOVE 10 TO WS1-I
               MOVE "s" TO QPR-DIALOG
           ELSE
               MOVE "y" TO QPR-DIALOG
           END-IF.
           ADD     1               TO  WS1-I.

      *********************
       PRINT-PAGES.
      *********************

           MOVE    LOW-VALUES      TO  CRE-DATA
           MOVE    LOW-VALUES      TO  CRE-FIELDS.
           MOVE    LOW-VALUES      TO  CRE-COLRS.
           MOVE    "CRE"          TO  CRE-NEXT-PANEL.

      * add logic here to fill variables declared in CRE-fields

           CALL    "QPR"           USING
                                   QPR-PRINT-PAGE
                                   CRE-CONVERSE-DATA.

      *******************
       END-PRINT.
      *******************

           CALL "QPR"  USING  QPR-END-PRINT
                              QPR-NULL-PARM.

