      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 3-MISS IMPRESSION CONTRATS MISSION          �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    3-MISS.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.
           COPY "FORM80.FC".

       DATA DIVISION.

       FILE SECTION.

           COPY "FORM80.FDE".

       WORKING-STORAGE SECTION.
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
      *    �  Constantes pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
 
      *    旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *    �  Variables pour ce programme  �
      *    읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

           COPY "IMPRLOG.REC".
           COPY "FICHE.REC".
           COPY "BANQP.REC".
           COPY "V-BASES.REC".
           COPY "DOC.REC".
           COPY "V-VAR.CPY".

       01  FORMULAIRE.
           02 FORM-LINE          PIC X(80) OCCURS 65.

       01  TEXTES                PIC X(19200).
           
           COPY "V-VH00.CPY".

       01  FORM-NAME.
           02 FILLER             PIC X(8) VALUE "FORMINT.".
           02 FORM-TYPE          PIC X.

       01  ECR-DISPLAY.
           02 HE-ZC PIC Z.Z.Z.


       LINKAGE SECTION.
      *컴컴컴컴컴컴컴
           COPY "V-LINK.CPY".
           COPY "PERSON.REC".
           COPY "REGISTRE.REC".
           COPY "CARRIERE.REC".
           COPY "CONTYPE.REC".
           COPY "CCOL.REC".
           COPY "MISSION.REC".
           COPY "POCL.REC".
           COPY "METIER.REC".
           COPY "RAISON.REC".
           COPY "CONTRAT.REC".

       PROCEDURE DIVISION USING LINK-V
                                PC-RECORD
                                PR-RECORD
                                REG-RECORD
                                CAR-RECORD
                                CCOL-RECORD
                                MISS-RECORD
                                RA-RECORD
                                CON-RECORD
                                MET-RECORD.

       DECLARATIVES.

       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON FORM.
       
       FILE-ERROR-PROC.
           CALL "C$RERR" USING EXTENDED-STATUS.
           CALL "0-ERROR" USING LINK-V.
           EXIT  PROGRAM.
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-3-MISS.


           MOVE 65 TO IMPL-MAX-LINE.
           MOVE LNK-TEXT TO FORM-TYPE.
           PERFORM READ-FORM.

           MOVE FORMULAIRE TO TEXTES.
           CALL "4-DOCFIL" USING LINK-V
                                 PR-RECORD
                                 REG-RECORD
                                 CAR-RECORD
                                 CON-RECORD
                                 CCOL-RECORD
                                 FICHE-RECORD
                                 MET-RECORD
                                 BASES-REMUNERATION
                                 DOC-RECORD
                                 TEXTES.

           CALL "4-DOCPC" USING LINK-V
                                 PC-RECORD
                                 TEXTES.

           CALL "4-DOCMIS" USING LINK-V
                                 MISS-RECORD
                                 PR-RECORD
                                 PC-RECORD
                                 TEXTES.


           MOVE TEXTES TO FORMULAIRE.



           MOVE  0 TO LNK-VAL.
           MOVE 60 TO LNK-LINE.
           CALL "PDOC" USING LINK-V FORMULAIRE.
           MOVE 99 TO LNK-VAL.
           CALL "PDOC" USING LINK-V FORMULAIRE.
           CANCEL "PDOC".

           EXIT PROGRAM.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XMESSAGE.CPY".
           COPY "XFORM.CPY".



