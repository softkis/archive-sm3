      *  旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
      *  � PROGRAMME 4-DOCMIS COMPLETER DOCUMENTS MISSIONS       �
      *  읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    4-DOCMIS.

       ENVIRONMENT DIVISION.

       CONFIGURATION SECTION.
      *---------------------
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
           
       DATA DIVISION.

       WORKING-STORAGE SECTION.

           COPY "MESSAGE.REC".
           COPY "CODSAL.REC".
           COPY "METIER.REC".
           COPY "RAISON.REC".
           COPY "PROF.REC".
           COPY "ECHEANCE.REC".

       01  IDX-1                 PIC 9(5).
       01  IDX-2                 PIC 9(5).
       01  IDX-3                 PIC 9(5).
       01  IDX-4                 PIC 9(5).
       01  TEST-TEXT             PIC X(6).
       01  TEST-TEXT-R REDEFINES TEST-TEXT.
           02 TEST-TEXT-1       PIC X.
           02 TEST-TEXT-2       PIC X.
           02 TEST-TEXT-3       PIC X(4).
       01  LINE-NUMBER           PIC 999.
       01  LIN-NUM               PIC 999.
       01  COL-NUM               PIC 999.
       01  DEC-NUM               PIC 999.
       01  EFFACER               PIC X(6) VALUE SPACES.
       01  REMPLACE              PIC X VALUE "�".
       01  action pic x.
       01  ALPHA-TEXTE PIC X(60).
       01  SH-00                PIC 9(8)V9(4).
       01  ESC-KEY               PIC 9(4) COMP-1.

       01  FAKE-KEY              PIC 9(4) COMP-1 VALUE 13.

       01  ECR-DISPLAY.
           02 HE-Z2 PIC ZZ  BLANK WHEN ZERO.
           02 HE-Z2Z2 PIC ZZ,ZZ BLANK WHEN ZERO.
           02 HE-Z3Z2 PIC ZZZ,ZZ BLANK WHEN ZERO.
           02 HE-Z3 PIC ZZZ BLANK WHEN ZERO.
           02 HE-Z4 PIC Z(4).
           02 HE-Z5 PIC Z(5).
           02 HE-Z6 PIC Z(6).
           02 HE-Z6Z2 PIC ZZ.ZZZ,ZZ.
           02 HE-Z8 PIC Z.ZZZ.ZZZ.
           02 HE-ZC PIC ZZZ.
           02 HE-M REDEFINES HE-ZC.
              03 HE PIC X OCCURS 3.
           02 HE-DATE.
              03 HE-JJ PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-MM PIC ZZ.
              03 FILLER PIC X VALUE ".".
              03 HE-AA PIC ZZZZ.
           02 HE-MATR.
              03 HM-AA PIC 9999.
              03 FILLER PIC X VALUE " ".
              03 HM-MM PIC 99.
              03 FILLER PIC X VALUE " ".
              03 HM-JJ PIC 99.
              03 FILLER PIC X VALUE " ".
              03 HM-SN PIC 999.

       LINKAGE SECTION.
      
           COPY "V-LINK.CPY".
           COPY "MISSION.REC".
           COPY "PERSON.REC".
           COPY "POCL.REC".
       01  TEXTES.
           02 FORM-LINE PIC X(19200).

       PROCEDURE DIVISION USING LINK-V
                                MISS-RECORD
                                PR-RECORD
                                PC-RECORD
                                TEXTES.


       START-DISPLAY SECTION.
             
       START-PROGRAMME-4-DOCMIS .


           MOVE 0 TO IDX-1.
           PERFORM TEST-FORM THRU TEST-FORM-END.
           EXIT PROGRAM.

       TEST-FORM.
           INSPECT TEXTES TALLYING IDX-1 FOR CHARACTERS BEFORE "&".
           IF  IDX-1 NOT = 0 
           AND IDX-1 < 19200
              PERFORM FILL-IN
           ELSE
              GO TEST-FORM-END
           END-IF.
           MOVE 0 TO IDX-1.
           GO TEST-FORM.
       TEST-FORM-END.
           INSPECT TEXTES REPLACING ALL "&" BY " ".

       FILL-IN.
           ADD 1 TO IDX-1.
           UNSTRING TEXTES INTO TEST-TEXT WITH POINTER IDX-1.
           SUBTRACT 6 FROM IDX-1.
           MOVE IDX-1 TO IDX-4.
           IF  TEST-TEXT-2 NOT = " "
           AND TEST-TEXT-2 NOT = "-"
               PERFORM ERASE-IT.
           EVALUATE TEST-TEXT
                WHEN "&DD   " PERFORM DOC-DAT
                WHEN "&DDN  " PERFORM DOC-DAT-N
      *         WHEN "&DA   " PERFORM DOC-AUT
                WHEN "&D1   " PERFORM MISS-DESC-1
                WHEN "&D2   " PERFORM MISS-DESC-2
                WHEN "&D3   " PERFORM MISS-DESC-3
                WHEN "&D4   " PERFORM MISS-DESC-4
                WHEN "&CON  " PERFORM MISS-NR
                WHEN "&REN  " PERFORM MISS-RENOUVEAU
                WHEN "&AD1  " PERFORM MISS-ADR-1
                WHEN "&AD2  " PERFORM MISS-ADR-2
                WHEN "&MET  " PERFORM MISS-METIER
                WHEN "&MOT  " PERFORM MISS-MOTIF
                WHEN "&HOR  " PERFORM MISS-HORAIRE
                WHEN "&HS   " PERFORM MISS-HRS-BASE
                WHEN "&HM   " PERFORM MISS-HRS-MOIS
                WHEN "&REM  " PERFORM MISS-REMP
                WHEN "&NAT  " PERFORM MISS-NAT
                WHEN "&CITP " PERFORM MISS-CITP
                WHEN "&LIEU " PERFORM MISS-LIEU
                WHEN "&ANNEE" PERFORM ANNEE
                WHEN "&MOIS " PERFORM MOIS
                WHEN "&MOIS1" MOVE 1 TO DEC-NUM
                              PERFORM MOIS-N
                WHEN "&MOIS2" MOVE 2 TO DEC-NUM
                              PERFORM MOIS-N
                WHEN "&MOIS3" MOVE 3 TO DEC-NUM
                              PERFORM MOIS-N
                WHEN "&MOIS4" MOVE 4 TO DEC-NUM
                              PERFORM MOIS-N

                WHEN "&DEB  " PERFORM MISS-DEBUT
                WHEN "&FIN  " PERFORM MISS-FIN
                WHEN "&DAT  " PERFORM MISS-DATE
                WHEN "&JRS  " PERFORM MISS-JRS
                WHEN "&ESS  " PERFORM MISS-ESSAI
                WHEN "&SAL  " PERFORM MISS-SAL
                WHEN "&REF  " PERFORM MISS-REF
                WHEN "&FAC  " PERFORM MISS-FACT
                WHEN "&PRO  " PERFORM MISS-PROF
                WHEN "&PAY  " PERFORM MISS-CONPAY
                WHEN "&PRIME" PERFORM MISS-PRIME
                WHEN "&FACT " PERFORM MISS-FACTURE
                WHEN "&ECH  " PERFORM ECHEANCE
                WHEN OTHER    PERFORM REMPLACE
           END-EVALUATE.

       DOC-DAT.
           MOVE MISS-EDIT-A TO HE-AA.
           MOVE MISS-EDIT-M TO HE-MM.
           MOVE MISS-EDIT-J TO HE-JJ.
           PERFORM STRING-DATE.

       DOC-DAT-N.
           MOVE MISS-EDIT-A TO HE-AA.
           MOVE MISS-EDIT-M TO HE-MM.
           MOVE MISS-EDIT-J TO HE-JJ.
           STRING HE-JJ DELIMITED BY SIZE 
           INTO TEXTES POINTER IDX-1.
           ADD 1 TO IDX-1.
           MOVE MISS-EDIT-M TO LNK-NUM.
           PERFORM MOIS-NOM-1.
           STRING ALPHA-TEXTE DELIMITED BY " " INTO TEXTES
           POINTER IDX-1 ON OVERFLOW CONTINUE END-STRING.
           ADD 1 TO IDX-1.
           STRING HE-AA DELIMITED BY SIZE INTO TEXTES POINTER IDX-1.

      *DOC-AUT.
      *    STRING DOC-AUTEUR DELIMITED BY SIZE
      *    INTO TEXTES POINTER IDX-1 
      *    ON OVERFLOW CONTINUE END-STRING.


       MISS-DESC-1.
           STRING MISS-DESC(1) DELIMITED BY "   " INTO TEXTES POINTER
           IDX-1 ON OVERFLOW CONTINUE END-STRING.
       MISS-DESC-2.
           STRING MISS-DESC(2) DELIMITED BY "   " INTO TEXTES POINTER
           IDX-1 ON OVERFLOW CONTINUE END-STRING.
       MISS-DESC-3.
           STRING MISS-DESC(3) DELIMITED BY "   " INTO TEXTES POINTER
           IDX-1 ON OVERFLOW CONTINUE END-STRING.
       MISS-DESC-4.
           STRING MISS-DESC(4) DELIMITED BY "   " INTO TEXTES POINTER
           IDX-1 ON OVERFLOW CONTINUE END-STRING.

       MISS-ADR-1.
           STRING MISS-ADRESSE-1 DELIMITED BY "   " INTO TEXTES POINTER
           IDX-1 ON OVERFLOW CONTINUE END-STRING.
       MISS-ADR-2.
           STRING MISS-ADRESSE-2 DELIMITED BY "   " INTO TEXTES POINTER
           IDX-1 ON OVERFLOW CONTINUE END-STRING.

       MISS-HORAIRE.
           STRING MISS-HORAIRE DELIMITED BY "   " INTO TEXTES POINTER
           IDX-1 ON OVERFLOW CONTINUE END-STRING.

       MISS-REMP.
           STRING MISS-REPLACE DELIMITED BY "   " INTO TEXTES POINTER
           IDX-1 ON OVERFLOW CONTINUE END-STRING.

       MISS-NAT.
           STRING MISS-NATURE DELIMITED BY "   " INTO TEXTES POINTER
           IDX-1 ON OVERFLOW CONTINUE END-STRING.

       MISS-LIEU.
           STRING MISS-L(1) DELIMITED BY "   " INTO TEXTES POINTER
           IDX-1 ON OVERFLOW CONTINUE END-STRING.

       MISS-CONPAY.
           STRING MISS-CONPAY DELIMITED BY "   " INTO TEXTES POINTER
           IDX-1 ON OVERFLOW CONTINUE END-STRING.

       MISS-NR.
           MOVE MISS-CONTRAT TO HE-Z6.
           STRING HE-Z6 DELIMITED BY SIZE INTO TEXTES POINTER IDX-1 
           ON OVERFLOW CONTINUE END-STRING.

       MISS-JRS.
           MOVE MISS-DUREE TO HE-Z4.
           STRING HE-Z4 DELIMITED BY SIZE INTO TEXTES POINTER IDX-1 
           ON OVERFLOW CONTINUE END-STRING.

       MISS-RENOUVEAU.
           STRING MISS-SUITE DELIMITED BY SIZE INTO TEXTES POINTER IDX-1 
           ON OVERFLOW CONTINUE END-STRING.

       MISS-ESSAI.
           MOVE MISS-ESSAI TO HE-Z4.
           STRING HE-Z4 DELIMITED BY SIZE INTO TEXTES POINTER IDX-1 
           ON OVERFLOW CONTINUE END-STRING.


       MISS-METIER.
           MOVE FR-LANGUE TO MET-LANGUE.
           MOVE MISS-METIER TO MET-CODE.
           CALL "6-METIER" USING LINK-V MET-RECORD FAKE-KEY.
           MOVE MET-NOM(PR-CODE-SEXE) TO ALPHA-TEXTE.
           PERFORM STRING-ALPHA.

       MISS-MOTIF.
           MOVE MISS-RAISON TO RA-CODE.
           CALL "6-RAISON" USING LINK-V RA-RECORD FAKE-KEY.
           MOVE RA-TEXTE(1) TO ALPHA-TEXTE.
           PERFORM STRING-ALPHA.

       MISS-DEBUT.
           MOVE MISS-DEBUT-A TO HE-AA.
           MOVE MISS-DEBUT-M TO HE-MM.
           MOVE MISS-DEBUT-J TO HE-JJ.
           PERFORM STRING-DATE.

       MISS-FIN.
           MOVE MISS-FIN-A TO HE-AA.
           MOVE MISS-FIN-M TO HE-MM.
           MOVE MISS-FIN-J TO HE-JJ.
           PERFORM STRING-DATE.

       MISS-DATE.
           MOVE MISS-EDIT-A TO HE-AA.
           MOVE MISS-EDIT-M TO HE-MM.
           MOVE MISS-EDIT-J TO HE-JJ.
           PERFORM STRING-DATE.

       STRING-DATE.
           STRING HE-DATE DELIMITED BY SIZE INTO TEXTES POINTER IDX-1.

       MISS-SAL.
           MOVE MISS-SAL-ACT TO HE-Z6Z2.
           PERFORM VALEUR.

       MISS-REF.
           MOVE MISS-SAL-REF TO HE-Z6Z2.
           PERFORM VALEUR.

       MISS-FACT.
           MOVE MISS-FACTURE TO HE-Z6Z2.
           PERFORM VALEUR.

       MISS-HJ.
           STRING MISS-HORAIRE DELIMITED BY SIZE INTO TEXTES POINTER
           IDX-1.

       MISS-PROF.
           MOVE MISS-PROF TO HE-Z4 PROF-CODE.
           STRING HE-Z4 DELIMITED BY SIZE INTO TEXTES POINTER IDX-1 
           ON OVERFLOW CONTINUE END-STRING.
           ADD 1 TO IDX-1.
           CALL "6-PROF" USING LINK-V PROF-RECORD FAKE-KEY.
           STRING PROF-NOM(1) DELIMITED BY "  "
           INTO TEXTES POINTER IDX-1 ON OVERFLOW CONTINUE END-STRING.

       MISS-CITP.
           MOVE MISS-PROF TO HE-ZC.
           STRING HE(1) DELIMITED BY SIZE INTO TEXTES POINTER IDX-1.
           ADD 3 TO IDX-1.
           STRING HE(2) DELIMITED BY SIZE INTO TEXTES POINTER IDX-1.
           ADD 3 TO IDX-1.
           STRING HE(3) DELIMITED BY SIZE INTO TEXTES POINTER IDX-1.

       MISS-HRS-BASE.
           MOVE MISS-HRS-BASE TO HE-Z2Z2.
           STRING HE-Z2Z2 DELIMITED BY SIZE INTO TEXTES POINTER IDX-1 
           ON OVERFLOW CONTINUE END-STRING.

       ANNEE.
           MOVE LNK-ANNEE TO HE-Z4.
           STRING HE-Z4 DELIMITED BY SIZE INTO TEXTES POINTER IDX-1 
           ON OVERFLOW CONTINUE END-STRING.

       MOIS.
           MOVE LNK-MOIS TO HE-Z2.
           STRING HE-Z2 DELIMITED BY SIZE INTO TEXTES POINTER IDX-1 
           ON OVERFLOW CONTINUE END-STRING.

       MOIS-N.
           PERFORM MOIS-NOM.
           PERFORM STRING-ALPHA.

       ERASE-IT.
           STRING EFFACER DELIMITED BY SIZE INTO TEXTES POINTER IDX-4 
           ON OVERFLOW CONTINUE END-STRING.

       REMPLACE.
           STRING REMPLACE DELIMITED BY SIZE INTO TEXTES POINTER IDX-4 
           ON OVERFLOW CONTINUE END-STRING.

       ECHEANCE.
           MOVE PC-ECHEANCE TO ECH-CODE.
           CALL "6-ECHEA" USING LINK-V ECH-RECORD FAKE-KEY.
           STRING ECH-NOM DELIMITED BY SIZE INTO TEXTES POINTER IDX-1
           ON OVERFLOW CONTINUE END-STRING.

       STRING-ALPHA.
           STRING ALPHA-TEXTE DELIMITED BY "  " INTO TEXTES
           POINTER IDX-1 ON OVERFLOW CONTINUE END-STRING.
       VALEUR.
           INSPECT HE-Z6Z2 REPLACING ALL ",00" BY "   ".
           STRING HE-Z6Z2 DELIMITED BY SIZE INTO TEXTES POINTER IDX-1 
           ON OVERFLOW CONTINUE END-STRING.


       MISS-PRIME.
           DIVIDE IDX-1 BY 80 GIVING LIN-NUM REMAINDER COL-NUM.
           PERFORM MAKE-PRIME THRU MAKE-PRIME-END
               VARYING IDX-2 FROM 1 BY 1 UNTIL IDX-2 > 10.

       MAKE-PRIME.
           IF MISS-CODE-PRIME(IDX-2) = 0
               GO MAKE-PRIME-END.
           IF  MISS-PRIME-ACT(IDX-2) = 0
           AND MISS-PRIME-PC(IDX-2)  = 0
               GO MAKE-PRIME-END.
           MOVE MISS-CODE-PRIME(IDX-2) TO CS-NUMBER.
           CALL "6-CODSAL" USING LINK-V CS-RECORD FAKE-KEY.
           MOVE CS-NOM TO  ALPHA-TEXTE.
           COMPUTE IDX-1 = 80 * LIN-NUM + COL-NUM.
           PERFORM STRING-ALPHA.
           COMPUTE IDX-1 = 80 * LIN-NUM + COL-NUM + 25.
           IF MISS-PRIME-ACT(IDX-2) NOT = 0
              MOVE MISS-PRIME-ACT(IDX-2) TO HE-Z6Z2
              PERFORM VALEUR
           END-IF.     
           COMPUTE IDX-1 = 80 * LIN-NUM + COL-NUM + 35.
           IF MISS-PRIME-PC(IDX-2) NOT = 0
              MOVE MISS-PRIME-PC(IDX-2)  TO HE-Z6Z2
              PERFORM VALEUR
              MOVE "%" TO ALPHA-TEXTE
              PERFORM STRING-ALPHA
           END-IF.
           MOVE 0 TO LNK-NUM.
           IF MISS-HOR-JOUR(IDX-2) = "H"
              MOVE 111 TO LNK-NUM.
           IF MISS-HOR-JOUR(IDX-2) = "J"
              MOVE 112 TO LNK-NUM.
           IF LNK-NUM NOT = 0
              COMPUTE IDX-1 = 80 * LIN-NUM + COL-NUM + 48
              MOVE "AA" TO LNK-AREA
              CALL "0-GMESS" USING LINK-V
              MOVE LNK-TEXT TO ALPHA-TEXTE
              PERFORM STRING-ALPHA
           END-IF.

           ADD 1 TO LIN-NUM.
       MAKE-PRIME-END.
           EXIT.

       MISS-HRS-MOIS.
           IF MISS-HOR-MEN = 0
              MOVE 111 TO LNK-NUM
           ELSE
              MOVE 114 TO LNK-NUM
           END-IF.
           MOVE "AA" TO LNK-AREA.
           CALL "0-GMESS" USING LINK-V.
           MOVE LNK-TEXT TO ALPHA-TEXTE.
           PERFORM STRING-ALPHA.

       MISS-FACTURE.
           DIVIDE IDX-1 BY 80 GIVING LIN-NUM REMAINDER COL-NUM.
           PERFORM MAKE-FACT THRU MAKE-FACT-END
               VARYING IDX-2 FROM 1 BY 1 UNTIL IDX-2 > 10.

       MAKE-FACT.
           IF MISS-CODE-PRIME(IDX-2) = 0
               GO MAKE-FACT-END.
           IF  MISS-FACT-PRIME(IDX-2) = 0
           AND MISS-PRIME-PC(IDX-2)  = 0
               GO MAKE-FACT-END.
           MOVE MISS-CODE-PRIME(IDX-2) TO CS-NUMBER.
           CALL "6-CODSAL" USING LINK-V CS-RECORD FAKE-KEY.
           MOVE CS-NOM TO  ALPHA-TEXTE.
           COMPUTE IDX-1 = 80 * LIN-NUM + COL-NUM.
           PERFORM STRING-ALPHA.
           COMPUTE IDX-1 = 80 * LIN-NUM + COL-NUM + 25.
           IF MISS-FACT-PRIME(IDX-2) NOT = 0
              MOVE MISS-FACT-PRIME(IDX-2) TO HE-Z6Z2
              PERFORM VALEUR
           END-IF.     
           COMPUTE IDX-1 = 80 * LIN-NUM + COL-NUM + 35.
           IF MISS-PRIME-PC(IDX-2) NOT = 0
              MOVE MISS-PRIME-PC(IDX-2)  TO HE-Z6Z2
              PERFORM VALEUR
              MOVE "%" TO ALPHA-TEXTE
              PERFORM STRING-ALPHA
           END-IF.
           MOVE 0 TO LNK-NUM.
           IF MISS-HOR-JOUR(IDX-2) = "H"
              MOVE 111 TO LNK-NUM.
           IF MISS-HOR-JOUR(IDX-2) = "J"
              MOVE 112 TO LNK-NUM.
           IF LNK-NUM NOT = 0
              COMPUTE IDX-1 = 80 * LIN-NUM + COL-NUM + 48
              MOVE "AA" TO LNK-AREA
              CALL "0-GMESS" USING LINK-V
              MOVE LNK-TEXT TO ALPHA-TEXTE
              PERFORM STRING-ALPHA
           END-IF.
           ADD 1 TO LIN-NUM.
       MAKE-FACT-END.
           EXIT.

           COPY "XMOISNOM.CPY".
           COPY "XACTION.CPY".
