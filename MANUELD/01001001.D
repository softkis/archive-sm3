

                      ORGANISATION DER LOHNARTEN
                                                                             
     Die Lohnarten sind ihrer Funktion nach aufgebaut, ->  Schema:
                                                                             
       1 ->  10 Basislohn                                                    
      11 ->  50 Lohnarten entsprechend den T�tigkeiten
      51 ->  99 Diverse Lohnarten
      80 ->  99 Naturallohn
     100 -> 130 �berstunden, + Zuschlag, plus Nacht-, Sonntag-, Feiertag
     131 -> 299 Diverse Lohnarten
     300 -> 320 Unregelm�ssige Bez�ge: Gratifikationen, Pr�mien, R�cklagen
     321 -> 325 Lohnauszahlung f�r nicht genommene Freistunden
     326 -> 349 Unregelm�ssige Bez�ge: Diverse
     350 -> 362 Kurzarbeit
     400 -> 410 Krankengeldberechnung
     421 -> 425 Lohnr�cknahme f�r zuviel ausgezahlte Freistunden
     450 -> 499 Bruttor�cknahme, Bu�gelder, Lohn im Ausland
     500 -> 599 Kosten- und Aufwandsentsch�digung
     600 -> 699 Nettoabz�ge
     701 -> 710 Krankengeldmerker
     711 -> 999 Hilfslohnarten
    1000 ->1999 Nettoausgleich +
    2000 ->2999 Nettoausgleich -


