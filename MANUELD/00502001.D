



                            LOHNSTEUERKORREKTUR
                           _____________________          
                                                                             

         Falls eine Lohnsteuerkarte mit Verzug abgegeben wird, kann eine
         Neubestimmung der Steuern notwendig sein.
         
         Das Programm enth�lt eine Routine die dieses Problem behebt.
         
         Das Prinzip: die Steuern der vorherigen Monate werden neugerechnet, 
         der Netto-Unterschied, positiv oder negativ, wird im laufenden 
         Monat dem Lohn beigef�gt bzw. davon abgezogen. Krankengeld, das
         vom Arbeitgeber vorgestreckt wurde, wird ber�cksichtigt.
         
         Die �nderungen werden auf dem Lohnzettel angezeigt: spezielle
         Lohnarten zeigen die abweichenden Nettobetr�ge monatsgenau an.
         
         Die Prozedur erfolgt in zwei Stufen:
         
         a) Den laufenden Monat einstellen, d.h. der n�chste zu zahlende
         Lohn. Im Programm "FICHE" wird die Steuerkarte, mit den richtigen
         Anwendungsdaten, Jahr und Monat, eingetragen.
         
         b) Im Programm "Steuerberichtigung" (RI) bestimmt man die
         Person und l�st mit der F5-Taste den Korrekturlauf aus. 
         Das ist alles.
         
         Das Programm ber�cksichtigt diese Ver�nderungen in der Journaldatei 
         der Finanzbuchhaltung sowie in der Lohnsteuererkl�rung.
         
         Diese Prozedur kann vor oder nach der Lohnerstellung laufen,
         aber auf jeden Fall vor den �berweisungen!
         
         (Das Programm erlaubt es: man riskiert, einen Restwert zu behalten 
         den man dann wieder auf einen Folgemonat �bertragen mu�, oder 
         man mu�, bei einem positiven Betrag, eine zus�tzliche �berweisung
         erstellen.)
         
         
         