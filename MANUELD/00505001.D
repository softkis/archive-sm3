


            LOHNNEBENKOSTENAUSGLEICH F�R NATURALLOHN
            ����������������������������������������
                
        Rechnung:
        
        A   = Vorteil im Netto-natura
        C1  = Kosten (SN1) - Kosten (SN2)
        C2  = Kosten (SN3) - Kosten (SN1)
        
        SN1 = Nettolohn
        SN2 = Nettolohn - Vorteil Netto-natura
        SN3 = SN1 + Ausgleich    
        SN4 = SN2 + Ausgleich
            
        Die A-Kosten = C1 sind definiert: Kosten (SN1) - Kosten (SN2)
        
        Die C2-Kosten sind definiert: Kosten (SN3) - Kosten (SN1).
        
        Beispiel:
        
        A               =     500
        
        Lohn SN1        = 100.000
        Lohn SN2        =  99.500
        
        Das Programm f�gt ein Brutto von 700 Franken zu, um vom Lohn 2
        (99.500) auf Lohn 1 (100.000) zu gelangen. Deshalb mu� man 
        700 - 500 = 200 Franken im Netto = C1 ausgleichen.
        
        SN3 = also 100.200.
        SN4 = also  99.700.
        
        Dies tr�gt dazu bei, da� sich ein definitiver Bruttoausgleich von 
        99.500 auf 100.200 Franken errechnet: man f�gt also einen 
        Bruttobetrag von 900 Franken hinzu. C2 = also 900 - 500 = 400.
               
        Das Programm f�gt dann anhand von einer Lohnart 400 Franken im
        Brutto bei und zieht 500 Franken im Netto ab.
         
        (Programmierbemerkung: Das Programm nimmt nur die Lohnarten an, 
        wo das Feld Einheitswert den Nettoteil enth�lt und in die Gruppe 13 
        = Nettoabz�ge geht und das Feld Gesamt den Bruttoanteil und somit 
        in die Gruppe 7 = Bruttolohn / Siehe Lohnbuchbeschreibung.)
        

        
        